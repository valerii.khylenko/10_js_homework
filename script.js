
const tabTitle = document.querySelectorAll('.tabs-title');
const tabItems = document.querySelectorAll('.tabs-content > li');

tabItems.forEach(elem => elem.classList.add('tab-items'));
// console.log (tabItems);
document.querySelector('.tabs').addEventListener('click', (event) => {
        tabTitle.forEach((el) => el.classList.remove('active'));
        event.target.classList.add('active');

        tabItems.forEach((elem) => {
            if (elem.dataset.list === event.target.dataset.list) {
                elem.classList.add('textDisplay');
            } else {
                elem.classList.remove('textDisplay');
            }
        });
    });